const readline = require("readline");

const userInput = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
});

const maxRandom = 100;

function startGame() {
    console.log("Coucou");
    userInput.question("Voulez-vous jouer ? (Y/n) : ", answer => {
        (answer.toUpperCase() === "Y") ? startGuess() : endGame();
    });
}

function startGuess() {
    const numberToGuess = Math.floor(Math.random() * maxRandom + 1);
    console.log("Devinez un nombre entre 0 et 100");
    userInput.prompt(true);
    userInput.on("line", (input) => {
        console.log(testUserInput(input, numberToGuess));
        userInput.prompt(true);
    });
}

function testUserInput(input, numberToGuess) {
    const pattern = /^\d/;

    if ( input.match(pattern) ) {
        input = parseInt(input, 10);
        numberToGuess = parseInt(numberToGuess, 10);
        switch (true) {
            case input > maxRandom || input < 1:
                return `ENTRE 1 et ${ maxRandom } on en parle ?`;
                break;
            // Égalité
            case input == numberToGuess:
                console.log("Bien joué!");
                process.exit();
            // userInput plus grands
            case input > numberToGuess:
                return ((input - numberToGuess) > 10)
                    ? "C'est beaucoup moins"
                    : ((input - numberToGuess) <= 5)
                        ? "C'est un tout petit peu moins" : "c'est un peu moins";
                break;
            // userInput plus petit
            case input < numberToGuess:
                return ((numberToGuess - input) > 10)
                    ? "C'est beaucoup plus"
                    : ((numberToGuess - input) <= 5)
                        ? "C'est un tout petit peu plus" : "c'est un peu plus";
                break;
            default:
        }
    } else {
        return "Faut pas écrire n'importe quoi !!!!";
    }

}

function endGame() {
    console.log("Bon ben salut!");
    process.exit();
}

startGame();
